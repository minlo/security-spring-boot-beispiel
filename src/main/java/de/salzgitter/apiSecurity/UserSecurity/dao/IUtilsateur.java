package de.salzgitter.apiSecurity.UserSecurity.dao;

import de.salzgitter.apiSecurity.UserSecurity.entity.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUtilsateur extends JpaRepository<Utilisateur, Long> {

   //@Query("select u from Utilisateur u where u.username = :username")
    Utilisateur findByUsername(final String username);
}
