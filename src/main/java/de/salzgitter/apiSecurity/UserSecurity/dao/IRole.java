package de.salzgitter.apiSecurity.UserSecurity.dao;

import de.salzgitter.apiSecurity.UserSecurity.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRole extends JpaRepository<Role, Long> {

    //@Query("select r from Role r where r.name = :name")
    Role findByName(final String name);
}
