package de.salzgitter.apiSecurity.UserSecurity;

import de.salzgitter.apiSecurity.UserSecurity.entity.Role;
import de.salzgitter.apiSecurity.UserSecurity.entity.Utilisateur;
import de.salzgitter.apiSecurity.UserSecurity.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

@SpringBootApplication
public class UserSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserSecurityApplication.class, args);

	}

	@Bean
	PasswordEncoder passwordEncoder(){

		return new BCryptPasswordEncoder();
	}

	@Bean
	CommandLineRunner run(UserService userService){
		return args -> {
			userService.saveRole(new Role(null, "ROLE_USER"));
			userService.saveRole(new Role(null, "ROLE_MANAGER"));
			userService.saveRole(new Role(null, "ROLE_ADMIN"));
			userService.saveRole(new Role(null, "ROLE_SUPER_ADMIN"));

			userService.saveUser(new Utilisateur(null, "John Travolta", "username1", "1234", new ArrayList<>()));
			userService.saveUser(new Utilisateur(null, "John", "username2", "0000", new ArrayList<>()));
			userService.saveUser(new Utilisateur(null, "Joseph", "username3", "1234", new ArrayList<>()));
			userService.saveUser(new Utilisateur(null, "JoJo", "username4", "1234", new ArrayList<>()));
			userService.saveUser(new Utilisateur(null, "David", "username5", "0001", new ArrayList<>()));

			userService.addRoletoUser("username1", "ROLE_USER");
			userService.addRoletoUser("username2", "ROLE_MANAGER");
			userService.addRoletoUser("username3", "ROLE_ADMIN");
			userService.addRoletoUser("username4", "ROLE_SUPER_ADMIN");
			userService.addRoletoUser("username5", "ROLE_USER");
		};
	}

}
