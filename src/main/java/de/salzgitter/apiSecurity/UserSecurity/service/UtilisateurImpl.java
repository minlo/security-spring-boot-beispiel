package de.salzgitter.apiSecurity.UserSecurity.service;

import de.salzgitter.apiSecurity.UserSecurity.dao.IRole;
import de.salzgitter.apiSecurity.UserSecurity.dao.IUtilsateur;
import de.salzgitter.apiSecurity.UserSecurity.entity.Role;
import de.salzgitter.apiSecurity.UserSecurity.entity.Utilisateur;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service @RequiredArgsConstructor @Transactional @Slf4j
public class UtilisateurImpl implements UserService, UserDetailsService {

    private final IUtilsateur userRepository;
    private final IRole rolerpository;
    private final PasswordEncoder  passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Utilisateur user = userRepository.findByUsername(username);
        if (user == null){
            log.error("User mit Username "+username+" don't exist in our system");
            throw new UsernameNotFoundException("User mit Username "+user.getUsername()+" don't exist in our system");
        }else {
            log.info("User found in our system {}", user.getName());

        }

        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();

        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        });

        return new org.springframework.security.core.userdetails.User(
                user.getUsername(),
                user.getPassword(),
                authorities);
    }

    @Override
    public Utilisateur saveUser(Utilisateur utilisateur) {
        log.info("saving new user {} to the db", utilisateur.getName());
        utilisateur.setPassword(passwordEncoder.encode(utilisateur.getPassword()));
        return userRepository.save(utilisateur);
    }

    @Override
    public Role saveRole(Role role) {
        log.info("saving new role {} to the db", role.getName());
        return rolerpository.save(role);
    }

    @Override
    public void addRoletoUser(String username, String roleName) {
        log.info("adding role {} to user {} ", roleName, username);
        Utilisateur user = userRepository.findByUsername(username);
        Role role = rolerpository.findByName(roleName);
        user.getRoles().add(role);

    }

    @Override
    public Utilisateur getUser(String username) {
        log.info("fetching user {}", username);
        return userRepository.findByUsername(username);
    }

    @Override
    public List<Utilisateur> getUsers() {
        log.info("fetchung all users from db");
        return userRepository.findAll();
    }

}
