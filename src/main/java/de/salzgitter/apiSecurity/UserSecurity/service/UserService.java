package de.salzgitter.apiSecurity.UserSecurity.service;

import de.salzgitter.apiSecurity.UserSecurity.entity.Role;
import de.salzgitter.apiSecurity.UserSecurity.entity.Utilisateur;

import java.util.List;

public interface UserService {

    /** Should save an User in DB */
    Utilisateur saveUser(Utilisateur utilisateur);

    /** Should save a role in DB */
    Role saveRole(Role role);

    /** Should add a role to an User in DB */
    void addRoletoUser(String username, String roleName);

    /** Should fetch an User by username from Db */
    Utilisateur getUser(String username);

    /** Should fetch all users from the  DB */
    List<Utilisateur> getUsers();
}
